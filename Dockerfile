FROM maven:3.6.3-jdk-11-slim

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package -DskipTests=true

ENV PORT 8080
EXPOSE $PORT
CMD [ "sh", "-c", "mvn spring-boot:run" ]
