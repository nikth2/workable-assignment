package assignment.movies.demo.converters;

import assignment.movies.demo.dto.MovieDto;
import assignment.movies.demo.model.MovieEntity;
import assignment.movies.demo.model.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.LocalDateTime;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class MovieEntityToDtoConverterTest {

    private MovieEntityToDtoConverter converter;
    MovieEntity entity;

    @BeforeEach
    void setup() {
        UserEntity user = new UserEntity();
        user.setUsername("testuser");
        entity = new MovieEntity();
        entity.setTitle("title");
        entity.setDescription("description");
        entity.setUser(user);
        entity.setPublicationDate(LocalDateTime.now());
        entity.setHates(new HashSet<>());
        entity.setLikes(new HashSet<>());
        entity.getHates().add(user);
        mockAuthentication();
    }

    @Test
    public void convert() {
        converter = new MovieEntityToDtoConverter();
        MovieDto dto = converter.convert(entity);
        assertEquals("title", dto.getTitle());
        assertEquals("description", dto.getDescription());
        assertEquals("testuser", dto.getUsername());
        assertEquals(1, dto.getHates());
        assertEquals(false, dto.getLikedByUser());
        assertEquals(true, dto.getHatedByUser());
    }

    @Test
    public void convert_testDates_1() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime modifedDateTime = now.minusDays(5);
        entity.setPublicationDate(modifedDateTime);
        converter = new MovieEntityToDtoConverter();
        MovieDto dto = converter.convert(entity);
        assertEquals("5 days ago", dto.getPublicationDateDifference());
    }

    @Test
    public void convert_testDates_2() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime modifedDateTime = now.minusDays(1);
        entity.setPublicationDate(modifedDateTime);
        converter = new MovieEntityToDtoConverter();
        MovieDto dto = converter.convert(entity);
        assertEquals("1 day ago", dto.getPublicationDateDifference());
    }

    @Test
    public void convert_testDates_3() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime modifedDateTime = now.minusDays(1);
        modifedDateTime = modifedDateTime.minusMonths(2);
        entity.setPublicationDate(modifedDateTime);
        converter = new MovieEntityToDtoConverter();
        MovieDto dto = converter.convert(entity);
        assertEquals("1 day 2 months ago", dto.getPublicationDateDifference());
    }

    private void mockAuthentication() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(authentication.getName()).thenReturn("testuser");
        when(authentication.isAuthenticated()).thenReturn(true);
    }
}
