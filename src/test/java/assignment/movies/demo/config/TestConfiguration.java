package assignment.movies.demo.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@org.springframework.boot.test.context.TestConfiguration
@EnableWebSecurity
public class TestConfiguration extends WebSecurityConfigurerAdapter  {

    @Bean
    public DataSource dataSource() {
        return Mockito.mock(DataSource.class);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/user/*", "/")
                .permitAll();
        http.csrf()
                .ignoringAntMatchers("/user/*", "/");
        http.headers()
                .frameOptions()
                .sameOrigin();
    }

}
