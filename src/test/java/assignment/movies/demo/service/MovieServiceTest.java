package assignment.movies.demo.service;

import assignment.movies.demo.converters.MovieEntityToDtoConverter;
import assignment.movies.demo.dto.MovieDto;
import assignment.movies.demo.model.MovieEntity;
import assignment.movies.demo.repository.MoviesRepository;
import assignment.movies.demo.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieServiceTest {

    @Mock
    MoviesRepository moviesRepository;
    @Mock
    UserRepository userRepository;
    private MovieEntityToDtoConverter movieEntityToDtoConverter;

    //@InjectMocks
    MovieService movieService;
    List<MovieEntity> movieEntities;

    @BeforeEach
    public void setup() {
        System.out.println("MovieServiceTest.setup");
        MockitoAnnotations.initMocks(this);
        movieEntityToDtoConverter = new MovieEntityToDtoConverter();
        this.movieService = new MovieService(moviesRepository, userRepository, movieEntityToDtoConverter);
        movieEntities = new ArrayList<>();
        MovieEntity entity1 = new MovieEntity();
        entity1.setTitle("title1");
        entity1.setDescription("description1");
        entity1.setPublicationDate(LocalDateTime.now());
        movieEntities.add(entity1);
        MovieEntity entity2 = new MovieEntity();
        entity2.setTitle("title2");
        entity2.setDescription("description2");
        entity2.setPublicationDate(LocalDateTime.now());
        movieEntities.add(entity2);
    }

    public void getAllMovies() {
        System.out.println("MovieServiceTest.getAllMovies:"+this.movieService);
        when(moviesRepository.findAll(Sort.by(Sort.Direction.DESC, "publicationDate"))).thenReturn(movieEntities);
        List<MovieDto> movieDtos = this.movieService.getAllMovies();
        assertEquals(movieDtos.size(), movieEntities.size());
        assertEquals(movieDtos.get(0).getTitle(), movieEntities.get(0).getTitle());
        assertEquals(movieDtos.get(0).getDescription(), movieEntities.get(0).getDescription());
    }

    @Test
    public void saveMovieNew() {
        System.out.println("MovieServiceTest.saveMovieNew:"+this.movieService);
        mockAuthentication();
        when(moviesRepository.findByTitle(any())).thenReturn(Optional.empty());
        MovieDto movieDto = new MovieDto();
        movieDto.setDescription("dummy decription");
        movieDto.setTitle("dummy title");
        movieService.saveMovie(movieDto);
        ArgumentCaptor<MovieEntity> captor = ArgumentCaptor.forClass(MovieEntity.class);
        verify(moviesRepository, times(1)).save(captor.capture());
        assertEquals("dummy decription", captor.getValue().getDescription());
        assertEquals("dummy title", captor.getValue().getTitle());
    }

    private void mockAuthentication() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(authentication.getName()).thenReturn("testuser");
        when(authentication.isAuthenticated()).thenReturn(true);
    }
}
