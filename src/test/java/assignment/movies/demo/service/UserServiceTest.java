package assignment.movies.demo.service;

import assignment.movies.demo.dto.UserDto;
import assignment.movies.demo.exception.UserAlreadyExistsException;
import assignment.movies.demo.model.MovieEntity;
import assignment.movies.demo.model.UserEntity;
import assignment.movies.demo.repository.MoviesRepository;
import assignment.movies.demo.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    @Mock
    UserRepository userRepository;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    MoviesRepository moviesRepository;

    @InjectMocks
    UserService userService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        userService = new UserService(userRepository, passwordEncoder, moviesRepository);
    }

    @Test
    public void registerNewUserAccount() {
        UserDto userDto = new UserDto();
        userDto.setUserName("testuser");
        userDto.setPassword("1234");
        when(passwordEncoder.encode("1234")).thenReturn("4321");
        userService.registerNewUserAccount(userDto);
        ArgumentCaptor<UserEntity> captor = ArgumentCaptor.forClass(UserEntity.class);
        verify(userRepository, times(1)).save(captor.capture());
        assertEquals("testuser", captor.getValue().getUsername());
        assertEquals("4321", captor.getValue().getPassword());
    }

    @Test
    public void registerNewUserAccountDuplicateError() {
        UserDto userDto = new UserDto();
        userDto.setUserName("testuser");
        userDto.setPassword("1234");
        when(passwordEncoder.encode("1234")).thenReturn("4321");
        when(userRepository.save(any())).thenThrow(DataIntegrityViolationException.class);
        assertThrows(UserAlreadyExistsException.class, () -> userService.registerNewUserAccount(userDto));
        verify(userRepository, times(1)).save(any());
    }

    @Test
    public void rateMovie() {
        mockAuthentication();
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername("testuser");
        userEntity.setPassword("1234");
        userEntity.setLikedMovies(new HashSet<>());
        userEntity.setHatedMovies(new HashSet<>());
        Long movieId = 1l;
        MovieEntity entity1 = new MovieEntity();
        entity1.setTitle("title1");
        entity1.setDescription("description1");
        entity1.setPublicationDate(LocalDateTime.now());

        when(moviesRepository.findById(movieId)).thenReturn(Optional.of(entity1));
        when(userRepository.findByUsername("testuser")).thenReturn(userEntity);
        userService.rateMovie(1l ,true);
        ArgumentCaptor<UserEntity> captor = ArgumentCaptor.forClass(UserEntity.class);
        verify(userRepository, times(1)).save(captor.capture());
        assertEquals("testuser", captor.getValue().getUsername());
        assertEquals("title1", captor.getValue().getLikedMovies().stream().findFirst().get().getTitle());
    }

    private void mockAuthentication() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(authentication.getName()).thenReturn("testuser");
        when(authentication.isAuthenticated()).thenReturn(true);
    }
}

