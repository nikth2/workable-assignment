package assignment.movies.demo.controller;

import assignment.movies.demo.config.TestConfiguration;
import assignment.movies.demo.dto.UserDto;
import assignment.movies.demo.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {  TestConfiguration.class})
@WebAppConfiguration
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        SecurityAutoConfiguration.class})
@Profile("test")
public class RegistrationControllerTest {

    private MockMvc mockMvc;
    @Mock
    private UserService userService;

    @Autowired
    WebApplicationContext context;

    @Autowired
    FilterChainProxy springSecurityFilterChain;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(new RegistrationController(userService))
                .apply(springSecurity(springSecurityFilterChain))
                .build();
    }

    @Test
    public void registerUserAccountNoErrors() throws Exception {

        this.mockMvc.perform(post("/user/register")
                .param("firstName","1st name")
                .param("lastName","last name")
                .param("userName","testuser")
                .param("password","1234")
                .param("matchingPassword","1234"))
                .andDo(print());
        ArgumentCaptor<UserDto> captor = ArgumentCaptor.forClass(UserDto.class);
        verify(userService, Mockito.times(1)).registerNewUserAccount(captor.capture());
        assert(captor.getValue() != null);
        assertEquals(captor.getValue().getFirstName(), "1st name");
        assertEquals(captor.getValue().getPassword(), "1234");
    }

    @Test
    public void registerUserAccountWithErrors() throws Exception {

        this.mockMvc.perform(post("/user/register")
                .param("firstName","1st name")
                .param("lastName","last name")
                .param("userName","")
                .param("password","1234")
                .param("matchingPassword","111"))
                .andExpect(status().isOk())
                .andExpect(view().name("registration"))
                .andExpect(model().attributeExists("userDto"))
                .andExpect(model().attributeHasFieldErrors("userDto","userName"))
                .andExpect(model().attributeHasFieldErrorCode("userDto","userName","NotEmpty"))
                .andDo(print());
        verify(userService, Mockito.times(0)).registerNewUserAccount(any());
    }
}
