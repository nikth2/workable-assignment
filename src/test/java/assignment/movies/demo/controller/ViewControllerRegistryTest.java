package assignment.movies.demo.controller;

import assignment.movies.demo.config.TestConfiguration;
import assignment.movies.demo.config.WebConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest( excludeAutoConfiguration = SecurityAutoConfiguration.class)
@ContextConfiguration(classes= {WebConfig.class})
public class ViewControllerRegistryTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testRootUrl() throws Exception {
        this.mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("forward:/movies/list"));
    }

    @Test
    public void testRegistrationUrl() throws Exception {
        this.mockMvc.perform(get("/registrationSuccess.jsp"))
                .andExpect(status().isOk())
                .andExpect(view().name("/movies/list"));
    }

    @Test
    public void testMoviesUrl() throws Exception {
        this.mockMvc.perform(get("/movies"))
                .andExpect(status().isOk())
                .andExpect(view().name("forward:/movies/list"));
    }

}
