package assignment.movies.demo.controller;

import assignment.movies.demo.config.WebConfig;
import assignment.movies.demo.dto.MovieDto;
import assignment.movies.demo.service.MovieService;
import assignment.movies.demo.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { WebConfig.class })
@WebAppConfiguration
public class MoviesControllerTest {

    private MockMvc mockMvc;
    @Mock
    private MovieService movieService;
    @Mock
    private UserService userService;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new MoviesController(movieService, userService)).build();
    }

    @Test
    public void testMoviesListUrl() throws Exception {
        ArrayList<MovieDto> movieDtos = new ArrayList<>();
        when(movieService.getAllMovies()).thenReturn(movieDtos);
        this.mockMvc.perform(get("/movies/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("movies"))
                .andExpect(model().attribute("movies", movieDtos));
        verify(movieService, Mockito.times(1)).getAllMovies();
    }

    @Test
    public void testMoviesListUrlWithUsername() throws Exception {
        ArrayList<MovieDto> movieDtos = new ArrayList<>();
        when(movieService.getAllMoviesForUser("test")).thenReturn(movieDtos);
        this.mockMvc.perform(get("/movies/list?username=test"))
                .andExpect(status().isOk())
                .andExpect(view().name("movies"))
                .andExpect(model().attribute("movies", movieDtos));
        verify(movieService, Mockito.times(1)).getAllMoviesForUser("test");
    }

    @Test
    public void testMoviesSortUrl() throws Exception {
        ArrayList<MovieDto> movieDtos = new ArrayList<>();
        when(movieService.getAllMovies()).thenReturn(movieDtos);
        this.mockMvc.perform(get("/movies/sort?type=hates"))
                .andExpect(status().isOk())
                .andExpect(view().name("movies"))
                .andExpect(model().attribute("movies", movieDtos));
        verify(movieService, Mockito.times(1)).getAllMovies("hates");
    }

    @Test
    public void testMoviesRateUrl() throws Exception {
        ArrayList<MovieDto> movieDtos = new ArrayList<>();
        when(movieService.getAllMovies()).thenReturn(movieDtos);
        this.mockMvc.perform(get("/movies/rate?movieId=1&like=true"))
                .andExpect(status().isOk())
                .andExpect(view().name("movies"))
                .andExpect(model().attribute("movies", movieDtos));
        verify(userService, Mockito.times(1)).rateMovie(1l, true);
    }

    @Test
    public void testMoviesNewUrl() throws Exception {
        this.mockMvc.perform(get("/movies/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("new_movie"))
                .andExpect(model().attributeExists("movieDto"));
    }

    @Test
    public void testNewMovieUrlNoErrors() throws Exception {
        this.mockMvc.perform(post("/movies/newMovie")
                .param("title", "test title")
                .param("description", "test description"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"))
                .andExpect(model().attributeExists("movieDto"));
        ArgumentCaptor<MovieDto> captor = ArgumentCaptor.forClass(MovieDto.class);
        verify(movieService, Mockito.times(1)).saveMovie(captor.capture());
        assert(captor.getValue() != null);
        assertEquals(captor.getValue().getTitle(), "test title");
        assertEquals(captor.getValue().getDescription(), "test description");
    }

    @Test
    public void testNewMovieUrlWithErrors() throws Exception {
        this.mockMvc.perform(post("/movies/newMovie")
                .param("description", "test description")
                .param("title", ""))
                .andExpect(status().isOk())
                .andExpect(view().name("new_movie"))
                .andExpect(model().attributeExists("movieDto"))
                .andExpect(model().attributeHasFieldErrors("movieDto","title"))
                .andExpect(model().attributeHasFieldErrorCode("movieDto","title","NotEmpty"));
        verify(movieService, Mockito.times(0)).saveMovie(any());
    }

}
