### MovieRama

This project is based on spring-mvc, runs on spring-boot and uses H2 embedded database for persistency, without an external volume.
That means that by restarting the application, all the new data are lost.  

You can run the application either with java -jar, or though a docker image.

### Build and Run the application with java

1. Clone the repository  
$ git clone https://gitlab.com/nikth2/workable-assignment.git
   
2. In the project directory run (skip tests for speed)  
$ mvn clean package -DskipTests=true
   
3. In the project directory run  
$ java -jar target\demo-0.0.1-SNAPSHOT-task.jar
   
*The project was built with maven 3.6 and java11.

### Build and run from docker

1. In the project directory run  
$ docker build -t movierama .

2. After (1) run  
$ docker run -p8080:8080 movierama


### Access the application
In any browser open http://localhost:8080/

#### You can also access the application [here](http://94.68.124.252:8888/), however this link is not always up.

### Database
The integration with the database is done with Spring JPA (hibernate).
The model of the database is created automatically based on the annotations found in classes under package assignment.movies.demo.model

To inspect the database schema and the generated data do:  
From any browser go to http://localhost:8080/h2-console/  
JDBC Url: "jdbc:h2:mem:testdb"  
username: sa  
no password  

The database is initialized on the startup of the application with a script.
A few movies are added, along with 2 users (username:{nikos, johndoe}, password:test)

